const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const removeEmpty = x => x.filter(y => !!y);
const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
  context: path.join(__dirname, 'src'),

  resolve: {
    extensions: ['.js', '.jsx']
  },

  entry: removeEmpty([
    'babel-polyfill',
    devMode ? 'react-hot-loader/patch' : undefined,
    // (devMode && process.env.NODE_ENV !== 'development') ? 'webpack/hot/only-dev-server' : undefined,
    devMode ? './index.dev.js' : './index.prod.js',
  ]),

  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'index.js',
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            extends: path.join(__dirname, '.babelrc')
          }
        }
      }, {
        test: /\.(png|jpg|svg|ttf|eot|woof|woof2)$/,
        use: [{
          loader: 'file-loader',
          options: {name: '[path][name].[ext]'}
        }]
      }
    ]
  },

  devtool: devMode ? 'inline-cheap-module-source-map' : 'inline-cheap-module-source-map',

  // devServer:{
  //   contentBase: __dirname + '/public',
  //   host: 'localhost',
  //   port: 8090,
  //   hot: true
  // },

  plugins: removeEmpty([
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new HtmlWebpackPlugin({
      title: 'My App',
      template: path.join(__dirname, 'src', 'index.html'),
    }),
    devMode ? new webpack.NamedModulesPlugin() : undefined,
    devMode ? new webpack.NoEmitOnErrorsPlugin() : undefined,
    new ExtractTextPlugin('style.css', {allChunks: true, disable: devMode})
  ])
}

module.exports.module.rules.push(
  {
    test: /\.(sa|sc|c)ss$/,
    // include: path.resolve(__dirname, './src/components'),
    use: devMode ?
      ['style-loader', 'css-loader', 'autoprefixer-loader?browsers=last 4 version', 'sass-loader'] :
      ExtractTextPlugin.extract({
        use: [
          {
            loader: 'css-loader',
            options: {
              minimize: true,
              sourceMap: true
            }
          },
          {loader: 'autoprefixer-loader?browsers=last 4 version'},
          {loader: 'sass-loader'}
        ]
      })
  }
);