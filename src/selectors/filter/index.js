import {createSelector} from 'reselect'

const getVisibilityFilter = (state) => state.visibilityFilterCombineReducers;
const getTasks = (state) => state.tasksCombineReducers.list;

export const getListCompleted = createSelector(
  [getVisibilityFilter, getTasks],
  (visibilityFilter, list) => {
    switch (visibilityFilter) {
      case 'all':
        return list;
      case 'active':
        return list.filter(t => !t.completed);
      case 'completed':
        return list.filter(t => t.completed);
    }

  }
);
