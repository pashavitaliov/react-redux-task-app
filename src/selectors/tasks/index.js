import { createSelector } from 'reselect'

const getTasks = (state) => state.list;

export const getCompletedTodo = createSelector(
  [getTasks],
  tasks => {
    return (
      tasks.reduce((count, task) => task.completed ? count + 1 : count, 0)
    )
  }
);