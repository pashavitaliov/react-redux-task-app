const logger2 = store => next => action => {
  console.group('Custom-logger ' + action.type);
  console.info('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  console.groupEnd(action.type);
  console.log(result);
  return result;
};
export default logger2