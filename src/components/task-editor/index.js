import React from 'react'
import TaskTextInput from '../task-text-input'
import PropTypes from 'prop-types'

const TaskEditor = (props) => {

  const {completeAllTask, addTask, tasksLength, tasksComplete} = props;

  return (

    <div className={'task__editor'}>
      <TaskTextInput
        tasksLength={tasksLength}
        tasksComplete={tasksComplete}
        completeAll={completeAllTask}
        newTask
        onSave={(text, color) => {
          if(text.length !== 0){
            addTask(text, color)
          }
        }}
        placeholder='Text plz'
      />
    </div>

  )
};

TaskEditor.propTypes = {
  addTask: PropTypes.func
};

export default TaskEditor;