import React from 'react'
import FilterItem from '../filter-item'
import {FILTER_BUTTONS} from '../../constants/task-filter'

const TaskFilter = (props) => (
    <div>
      TaskFilter
      <ul>
        {FILTER_BUTTONS.map(filter => (
          <FilterItem key={filter.id} {...filter} setFilter={props.setFilter}/>
        ))}
      </ul>
    </div>
  );

export default TaskFilter;