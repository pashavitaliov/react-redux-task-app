import React, {Component, Fragment} from 'react'
import PropTypes from 'prop-types'

class TaskTextEditor extends Component {

  static propTypes = {
    onSave: PropTypes.func,
    placeholder: PropTypes.string,
    newTask: PropTypes.bool,
    completeAll: PropTypes.func,
    tasksComplete: PropTypes.number,
    tasksLength: PropTypes.number
  };

  state = {
    text: this.props.text || '',
    color: '#000000'
  };

  onHandlerChangeEditor = (e) => {
    this.setState({ text: e.target.value });
  };
  onHandlerChangeColor = (e) => {
    this.setState({ color: e.target.value });
  };
  onHandlerAdd = () => {
    let newText = this.state.text.trim();
    let newColor = this.state.color;
    this.props.onSave(newText, newColor);
    this.setState({ text: '', color: '#000000' })
  };
  onHandlerKeyAdd = e => {
    if ( e.which === 13 ) {
      this.onHandlerAdd()
    }
  };
  handleBlur = e => {
    if ( !this.props.newTask ) {
      this.props.onSave(e.target.value)
    }
  };

  render() {
    const {tasksLength, tasksComplete, placeholder, completeAll} = this.props;

    let editor;

    if ( this.props.newTask ) {
      editor = (
        <Fragment>
          <input type="checkbox" checked={tasksLength === tasksComplete && tasksLength !== 0}  onChange={completeAll} />
          <input
            autoFocus
            type="text"
            placeholder={placeholder}
            onChange={this.onHandlerChangeEditor}
            onKeyPress={this.onHandlerKeyAdd}
            value={this.state.text}
            onBlur={this.handleBlur}/>
          <button onClick={this.onHandlerAdd}>Add</button>
          <input value={this.state.color} type="color" onChange={this.onHandlerChangeColor}/>
        </Fragment>
      )
    } else {
      editor = (
        <Fragment>
          <input
            autoFocus
            type="text"
            placeholder={placeholder}
            onChange={this.onHandlerChangeEditor}
            onKeyPress={this.onHandlerKeyAdd}
            value={this.state.text}
            onBlur={this.handleBlur}/>
          {/*<input value={this.state.color} type="color" onChange={this.onHandlerChangeColor}/>*/}
        </Fragment>
      )
    }

    return (
      <Fragment>
        {editor}
      </Fragment>
    )
  }
}

export default TaskTextEditor;