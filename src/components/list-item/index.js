import React, {Component, Fragment} from 'react'
import PropTypes from 'prop-types'
import TaskTextInput from '../task-text-input'

class ListItem extends Component {

  static propTypes = {
    text: PropTypes.string,
    id: PropTypes.number,
    color: PropTypes.string,
    completed: PropTypes.bool,
    deleteTask: PropTypes.func,
    completeTask: PropTypes.func
  };

  state = {
    editing: true
  };

  onHandlerSave = (id, text, color) => {
    if ( text.length === 0 ) {
      this.props.deleteTask(id);
    } else {
      this.props.editTask(text, id, color)
    }
    this.setState({ editing: true })
  };
  onEditing = () => {
    this.setState({ editing: false })
  };

  render() {

    const { text, id, color, completed } = this.props;
    const { deleteTask, completeTask } = this.props;
    const editing = this.state.editing;
    let element;

    if ( editing ) {
      element = (
        <Fragment>
          <input onChange={completeTask.bind(null, id)} type="checkbox" checked={completed}/>
          <span key={id} onDoubleClick={this.onEditing} style={{ color: color }}>{text}</span>
          <button onClick={() => deleteTask(id)}>x</button>
        </Fragment>
      )
    } else {
      element = (
        <TaskTextInput text={text} onSave={(text, color) => this.onHandlerSave(id, text, this.props.color)}/>
      )
    }

    return (
      <li>
        {element}
      </li>
    )
  }
}


export default ListItem;