import React from 'react'
import ListItem from '../list-item'
import PropTypes from 'prop-types'

const TaskList = ({ tasks, actions, testTasksList }) => {
  let tasksList = (
    testTasksList.map(item => (
      <ListItem key={item.id} {...actions} {...item} />
    ))
  );
  return (
  <ul>
    {tasksList}
  </ul>
  )
};

TaskList.propTypes = {
  tasks: PropTypes.array,
  actions: PropTypes.object
};

export default TaskList;
