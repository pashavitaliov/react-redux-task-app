import React from 'react'

const a = (t) => {
  console.log(t);
}

const FilterItem = (props) => {
  // console.log(props);
  return (
    <li>
      <a href={"#/" + props.status} onClick={() => props.setFilter(props.status)} >{props.text}</a>
    </li>
  );
}
export default FilterItem;