export const FILTER_BUTTONS = [
  {
    status : 'all',
    text : 'Все',
    id: 1
  },
  {
    status : 'active',
    text : 'Активные',
    id: 2
  },
  {
    status : 'completed',
    text : 'Закрытые',
    id: 3
  }
];