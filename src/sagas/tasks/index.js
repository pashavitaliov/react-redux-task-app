import {all, call, put, takeEvery, select} from 'redux-saga/effects'
import {ADD_TASK,
  DELETE_TASK,
  EDIT_TASK,
  COMPLETE_TASK,
  COMPLETE_ALL_TASKS,
  CLEAR_COMPLETED} from '../../constants/task'
import {returnLocalTasks} from "../../actions/tasks";

let actionsArr = [ADD_TASK,DELETE_TASK,EDIT_TASK,COMPLETE_TASK,COMPLETE_ALL_TASKS,CLEAR_COMPLETED];

function* localTasks() {
  try {
    const returnTasks = yield call(() => {
      if ( localStorage.getItem('tasksLocal') ) {
        return JSON.parse(localStorage.getItem('tasksLocal'));
      } else {
        throw new Error();
      }
    });
    yield put(returnLocalTasks(returnTasks.list));
  } catch ( err ) {
    yield put({ type: 'default' });
  }
}

function* updateLocalStorage(){
  const state = yield select();
  console.log(state)
  localStorage.setItem('tasksLocal', JSON.stringify(state.tasksCombineReducers));
  state.tasksCombineReducers.list.length === 0 ? localStorage.clear() : null;
}


function* tasksSaga() {
  yield takeEvery(actionsArr, updateLocalStorage);
}

function* rootSage() {
  yield all([localTasks(), tasksSaga()])
}

export default rootSage;