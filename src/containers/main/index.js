import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {deleteTask, editTask, completeTask} from '../../actions/tasks'

import TaskList from '../../components/task-list'

import { getListCompleted } from '../../selectors/filter'

const mapStateToProps = state => ({
  tasks: state.tasksCombineReducers.list,
  testTasksList: getListCompleted(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({deleteTask, editTask, completeTask}, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(TaskList);