import React from 'react'
import TaskFilter from '../../components/task-filter'
import {connect} from 'react-redux'
import {clearComplete, setVisibilityFilter} from '../../actions/tasks'
import {getCompletedTodo} from '../../selectors/tasks'

const Footer = (props) => {

  const {tasksLength, tasksComplete} = props;
  const active = tasksLength - tasksComplete;

  return (
    <div className={'test'}>
      <TaskFilter setFilter={props.setVisibilityFilter}/>
      <span>{tasksLength} {tasksLength > 1 ? 'Items ' : 'Item '}</span>
      <span>{tasksComplete} {'Completed '}</span>
      <span>{active} {'Active'}</span>
      {tasksComplete ? <div><button onClick={props.clearComplete}>Clear completed</button></div> : null}
    </div>
  );
};

const mapStateToProps = (state) => ({
    tasksLength: state.tasksCombineReducers.list.length,
    tasksComplete: getCompletedTodo(state.tasksCombineReducers)
  });

export default connect(mapStateToProps, {clearComplete, setVisibilityFilter})(Footer);