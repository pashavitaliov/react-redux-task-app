import React from 'react'
import {connect} from 'react-redux'
import {addTask, completeAllTask} from '../../actions/tasks'
import TaskEditor from '../../components/task-editor'
import {getCompletedTodo} from '../../selectors/tasks'

const mapStateToProps = (state) => {
  console.log(state);
  return ({

    tasksLength: state.tasksCombineReducers.list.length,
    tasksComplete: getCompletedTodo(state.tasksCombineReducers)
  })
};

export default connect(mapStateToProps, {addTask, completeAllTask})(TaskEditor);