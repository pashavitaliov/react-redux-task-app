import * as type from '../../constants/task'

export const addTask = (text, color) => ({type: type.ADD_TASK, text, color});
export const deleteTask = (id) => ({type: type.DELETE_TASK, id});
export const editTask = (text, id, color) => ({type: type.EDIT_TASK, text, id, color});
export const completeTask = (id) => ({type: type.COMPLETE_TASK, id});
export const completeAllTask = () => ({type: type.COMPLETE_ALL_TASKS});
export const clearComplete = () => ({type: type.CLEAR_COMPLETED});
export const returnLocalTasks = (list) => ({type: type.RETURN_LOCAL_TASKS, list});
export const setVisibilityFilter = (filter) => ({type: type.SET_VISIBILITY_FILTER, filter});