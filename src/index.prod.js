import React from 'react';
import {render} from 'react-dom';
import {createStore, applyMiddleware} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {Provider} from 'react-redux'
import reducer from './reducers'
import TaskApp from './page/task-app'
import mySaga from './sagas/tasks'
const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(mySaga);

render(
  <Provider store={store}>
    <TaskApp/>
  </Provider>,
  document.getElementById('app')
);