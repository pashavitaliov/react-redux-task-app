import React from 'react'
import { hot } from 'react-hot-loader'
import Header from '../../containers/header'
import Main from '../../containers/main'
import Footer from '../../containers/footer'

const TaskApp = () => {
  return (
    <div>
      <Header/>
      <Main/>
      <Footer/>
    </div>
  )
};

export default hot(module)(TaskApp);