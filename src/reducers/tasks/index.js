import * as type from '../../constants/task'

const initialState = {
  list: [
      {
        id: 0,
        completed: false,
        text: 'test text',
        color: '#000000'
      }, {
        id: 1,
        completed: false,
        text: '1111test text',
        color: '#000000'
      }
  ]
};

export default function list(state = initialState, action) {

  switch (action.type) {
    case type.ADD_TASK:
      return {
        list: [
          ...state.list,
          {
            id: state.list.reduce((maxId, task) => Math.max(task.id, maxId), -1) + 1,
            // id: Date.now(),
            completed: false,
            text: action.text,
            color: action.color.length ? action.color : '#000000'
          }
        ]

      };

    case type.DELETE_TASK:
      return {
        list: state.list.filter(task => ( task.id !== action.id ))
      };

    case type.EDIT_TASK:
      return {
        list:
          state.list.map(t => {
            if (t.id === action.id) {
              t.text = action.text;
              t.color = action.color;
            }
            return t;
          })

      };

    case type.COMPLETE_TASK:
      return {
        list:
          state.list.map(t => {
            if (t.id === action.id) {
              t.completed = !t.completed;
            }
            return t;
          })

      };

    case type.COMPLETE_ALL_TASKS:
      let areAllMarked = state.list.every(t => t.completed);
      return {
        list: state.list.map(t => {
            t.completed = !areAllMarked;
            return t;
          })
      };

    case type.CLEAR_COMPLETED:
      return {
        list: state.list.filter(t => (!t.completed))
      };


    case type.RETURN_LOCAL_TASKS:
      return {list: action.list};

    default:
      return state
  }

}