import {SET_VISIBILITY_FILTER} from '../../constants/task'

const initialState = 'all';

export default function visibilityFilter(state = initialState, action) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state
  }
};