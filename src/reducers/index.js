import {combineReducers} from 'redux'
import tasks from './tasks'
import visibilityFilter from './filters'


const rootReducer = combineReducers({
  tasksCombineReducers: tasks,
  visibilityFilterCombineReducers: visibilityFilter
});

export default rootReducer;